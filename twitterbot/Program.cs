﻿using System;
using System.Collections.Generic;
using Tweetinvi.Models;

namespace twitterbot
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            bool work = true;
            TwitterAuth twitterAuth = TwitterAuth.GetInstance(); //Подключаем приложение твиттера к программе.
            Account acc = new Account();
            Reader reader = new Reader();
            IEnumerable<ITweet> tweets;
            while (work)
            {
                Console.WriteLine("Введите имя пользователя или '/quit' для выхода:");
                string username = Console.ReadLine();
                if (username != "/quit")
                {
                    try 
                    {
                        acc.SetUserName(username); // устанавливаем указанное пользователем имя.
                        reader.SetAccount(acc); // устанавливаем для ридера аккаунт пользователя из которого нужно читать.
                        tweets = reader.Read(5); // читаем последние 5 твитов.
                        Statistic stats = new Statistic(tweets); 
                        Dictionary<char, double> statistic = stats.GetStatistic(); // считаем статистику по прочитанным 5 твитам.
                        Writer writer = new Writer(statistic, acc); // Инициализируем экземпляр класса записывающего статистику.
                        writer.Write(new TwitterWriter()); // пишем статистику используя TwitterWriter
                        writer.Write(new ConsoleWriter()); // пишем статистику используя ConsoleWriter
                    }
                    catch(Exception e) // при получении исклучения - выводим его пользователю и переходим ко вводу нового имени пользователя.
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                else
                {
                    work = false;
                }
            }
        }
    }
}
