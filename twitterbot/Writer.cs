﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace twitterbot
{
    public class Writer
    {
        /// <summary>
        /// Статистика по твитам.
        /// </summary>
        private Dictionary<char, double> _stats;

        /// <summary>
        /// Аккаунт пользователя.
        /// </summary>
        private Account _account;

        /// <summary>
        /// Максимальный размер твита.
        /// </summary>
        /// <value>Максимальный размер твита.</value>
        private int MaxLength 
        {
            get 
            {
                return 280;  
            }
        }

        /// <summary>
        /// Сообщение по умолчанию вставляемое в начале каждого твита.
        /// </summary>
        /// <value>Сообщение по умолчанию.</value>
        private string DefaultMessage 
        {
            get 
            {
                return this._account.username + ", статистика для последних 5 твитов: ";
            }
        }

        public Writer(Dictionary<char, double> stats, Account account)
        {
            this.SetStats(stats);
            this.SetAccount(account);
        }

        /// <summary>
        /// Устанавливает статистику.
        /// </summary>
        /// <param name="stats">Статистика.</param>
        public void SetStats(Dictionary<char, double> stats) 
        {
            this._stats = stats;
        }

        /// <summary>
        /// Устанавливает аккаунт пользователя.
        /// </summary>
        /// <param name="account">Аккаунт.</param>
        public void SetAccount(Account account) 
        {
            this._account = account;
        }

        /// <summary>
        /// Преобразует статистику в JSON-строку.
        /// </summary>
        /// <returns>JSON-строка.</returns>
        private string StatsToJson()
        {
            return JsonConvert.SerializeObject(this._stats);
        }

        /// <summary>
        /// Преобразует статистику в список JSON-строк.
        /// </summary>
        /// <returns>Список JSON-строк.</returns>
        /// <param name="chunk">По сколько символов разделять статистику.</param>
        private List<string> StatsToJson(int chunk) 
        {
            List<string> jsons = new List<string>();
            int skip = 0;
            while(skip < this._stats.Count) 
            {
                Dictionary<char, double> chunked = this.ToDictionary(this._stats.Skip(skip).Take(chunk)); // Преобразуем полученный результат в Dictionary
                jsons.Add(this.DefaultMessage + JsonConvert.SerializeObject(chunked));
                skip += chunk;
            }
            return jsons;
        }

        /// <summary>
        /// Преобразует IEnumerable в Dictionary.
        /// </summary>
        /// <returns>Возвращает Dictionary.</returns>
        /// <param name="collection">IEnumerable.</param>
        private Dictionary<char, double> ToDictionary(IEnumerable<KeyValuePair<char, double>> collection) 
        {
            Dictionary<char, double> dict = new Dictionary<char, double>();
            foreach(KeyValuePair<char, double> item in collection) 
            {
                dict.Add(item.Key, item.Value);
            }
            return dict;
        }

        /// <summary>
        /// Подготавливает статистику к преобразованию в список строк.
        /// </summary>
        /// <returns>Список строк.</returns>
        private List<string> StatsToString() 
        {
            List<string> statsString = new List<string>();
            string strStats = this.StatsToJson();
            if((strStats.Length + this.DefaultMessage.Length) > this.MaxLength) 
            {
                double diff = Math.Ceiling((double)(strStats.Length - this.DefaultMessage.Length) / this.MaxLength)+1; // высчитываем разницу в длине JSON и допустимой длине.
                int chunk = (int)Math.Ceiling((double)this._stats.Count / diff); // подсчитываем по сколько символов допустимо разделить статистику, чтобы строки вместились в максимальную длину.
                statsString = this.StatsToJson(chunk); // получаем список строк статистики
            }
            else 
            {
                statsString.Add(this.DefaultMessage + strStats);
            }
            return statsString;
        }

        /// <summary>
        /// Записывает статистику указанным способом.
        /// </summary>
        /// <returns>Записывает статистику.</returns>
        /// <param name="writer">Способ, которым нужно записать статистику.</param>
        public void Write(IWriter writer) 
        {
            if(this._stats.Count > 0) 
            {
                writer.Write(this.StatsToString());  
            }
            else 
            {
                throw new Exception("Статистика пуста.");
            }
        }
    }
}