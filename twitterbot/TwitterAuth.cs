﻿using System;
using System.Configuration;
using Tweetinvi;

namespace twitterbot
{
    public class TwitterAuth
    {

        /// <summary>
        /// Экземпляр класса.
        /// </summary>
        private static TwitterAuth _instance;

        private string _consumerKey = "Qa2cVYgHk6aAdHUASHKRzWxHy";
        private string _consumerSecret = "bGaCshNdfwdNBxbJ40WvsbxSedgaYQ1LUEhP06WexWo88QsuNj";
        private string _accessToken = "980458128234942465-FYzxc7BkF0CLFkOpD0A1FVb6nve0zfR";
        private string _accessSecret = "xnlwdoFBqlklVyokx6f55cwNyQJAA8Wv5CbKPUo7GucZk";

        private TwitterAuth()
        {
            //читаем настройки из app.config
            this._consumerKey = ConfigurationManager.AppSettings["consumer_key"];
            this._consumerSecret = ConfigurationManager.AppSettings["consumer_secret"];
            this._accessToken = ConfigurationManager.AppSettings["access_token"];
            this._accessSecret = ConfigurationManager.AppSettings["access_token_secret"];
            //настраиваем приложение твиттера
            this.SetUpTwitterAPI();
        }

        /// <summary>
        /// Возвращает экземпляр класса если он уже создан.
        /// </summary>
        /// <returns>Экземпляр класса.</returns>
        public static TwitterAuth GetInstance() 
        {
            if(TwitterAuth._instance == null) 
            {
                TwitterAuth._instance = new TwitterAuth();
            }
            return TwitterAuth._instance;
        }

        /// <summary>
        /// Настраивает приложение твитера.
        /// </summary>
        private void SetUpTwitterAPI()
        {
            Auth.SetUserCredentials(this._consumerKey, this._consumerSecret, this._accessToken, this._accessSecret);
        }

    }
}
