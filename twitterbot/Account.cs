﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace twitterbot
{
    public class Account
    {
        /// <summary>
        /// Символ с которого начинается имя пользователя.
        /// </summary>
        private char _atChar = '@';

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        private string _username;
        /// <summary>
        /// Хранит имя пользователя. При присваивании значения оно приводится к стандартному виду '@username'.
        /// </summary>
        /// <value>Имя пользователя.</value>
        public string username
        {
            get
            {
                try
                {
                    return this._username;
                }
                catch(StackOverflowException e)
                {
                    return e.Message;
                }
            }
            private set
            {
                int countAt = Regex.Matches(value, "@").Count;
                if(countAt > 1) 
                {
                    this._username = null;
                    throw new Exception("Символ '@' должен быть единственным в начале имени пользователя.");
                }
                if(value.Length <= 0)
                {
                    this._username = null;
                    throw new Exception("Имя пользователя не может быть пустым.");
                }
                if(value.Length == 1 && value[0] == '@')
                {
                    this._username = null;
                    throw new Exception("Имя пользователя не может состоять только из символа '@'.");
                }
                if (value.IndexOf(this._atChar) == 0)
                {
                   this._username = value;
                }
                else if (value.IndexOf(this._atChar) == -1)
                {
                    this._username = value.Insert(0, "@");
                }
                else
                {
                    this._username = null;
                    throw new Exception("Символ '@' должен находится в начале строки или отсутствовать.");
                }  
            }
        }

        public Account(string username)
        {
            this.SetUserName(username);
        }

        public Account() { }

        /// <summary>
        /// Устанавливает имя пользователя.
        /// </summary>
        /// <param name="uName">Имя пользователя.</param>
        public void SetUserName(string uName)
        {
            this.username = uName;
        }

    }
}
