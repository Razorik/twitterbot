﻿using System;
using System.Collections.Generic;

namespace twitterbot
{
    public class ConsoleWriter : IWriter
    {
        public ConsoleWriter(){}


        /// <summary>
        /// Выводит текст в консоль.
        /// </summary>
        /// <returns>Выводит в консоль текст.</returns>
        /// <param name="text">Текст который необходимо вывести в консоль.</param>
        public void Write(string text) 
        {
            Console.WriteLine(text);
        }


        /// <summary>
        /// Выводит список текстов в консоль.
        /// </summary>
        /// <returns>Выводит список текстов в консоль.</returns>
        /// <param name="texts">Список текстов, которые необходимо вывести.</param>
        public void Write(List<string> texts) 
        {
            texts.ForEach(this.Write);
        }
    }
}
