﻿using System;
using System.Collections.Generic;

namespace twitterbot
{
    public interface IWriter
    {
        void Write(string text);

        void Write(List<String> texts);
    }
}
