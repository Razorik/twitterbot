﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Parameters;
using Tweetinvi.Models;

namespace twitterbot
{
    class Reader
    {

        private Account _account;

        public Reader()
        {
        }

        public Reader(Account acc)
        {
            this.SetAccount(acc);
        }

        /// <summary>
        /// Устанавливает аккаунт пользователя в твиттере из которого необходимо читать твиты.
        /// </summary>
        /// <param name="acc">Acc.</param>
        public void SetAccount(Account acc)
        {
            this._account = acc;
        }

        /// <summary>
        /// Читает последние твиты из аккаунта пользователя.
        /// </summary>
        /// <returns>Возвращает прочитанные твиты.</returns>
        /// <param name="count">Количемтво твитов, которые нужно прочитать</param>
        public IEnumerable<ITweet> Read(int count)
        {
            UserTimelineParameters userTimelineParameters = new UserTimelineParameters();
            userTimelineParameters.MaximumNumberOfTweetsToRetrieve = count; // устанавливаем сколько твитов нужно прочитать
            try
            {
                return Timeline.GetUserTimeline(this._account.username, userTimelineParameters);
            }
            catch
            {
                throw new Exception("Некорректное имя пользователя.");
            }
        }

    }
}
