﻿using System;
using System.Collections.Generic;
using Tweetinvi.Models;

namespace twitterbot
{
    public class Statistic
    {

        /// <summary>
        /// Список символов которые встречаются в твитах
        /// </summary>
        private List<char> _chars;

        public Statistic(IEnumerable<ITweet> tweets)
        {
            this.Prepare(tweets); 
        }

        /// <summary>
        /// Преобразует твиты в список символов которые в них встречаются
        /// </summary>
        /// <returns>The prepare.</returns>
        /// <param name="tweets">Tweets.</param>
        public void Prepare(IEnumerable<ITweet> tweets) 
        {
            if(tweets != null)
            {
                string tweetsStr = "";
                foreach (ITweet tweet in tweets)
                {
                    tweetsStr += tweet.FullText;
                }
                this._chars = this.CharArrayToList(tweetsStr.ToLower().ToCharArray()); // преобразуем полученный массив символов в список
                if(tweetsStr.Length <= 0) 
                {
                    throw new Exception("Для указанного пользователя твиты не найдены."); 
                }
            }
            else 
            {
                throw new Exception("Для указанного пользователя твиты не найдены.");
            }
        }

        /// <summary>
        /// Преобразуем массив символов в список буквенных символов.
        /// </summary>
        /// <returns>Возвращает список буквенных символов.</returns>
        /// <param name="charsArr">Массив символов.</param>
        private List<char> CharArrayToList(char[] charsArr) 
        {
            List<char> listChar = new List<char>();
            foreach(char symbol in charsArr) 
            {
                if(Char.IsLetter(symbol)) 
                {
                    listChar.Add(symbol);  
                }
            }
            return listChar;
        }

        /// <summary>
        /// Собирает статистику по твитам.
        /// </summary>
        /// <returns>Вощзвращает статистику.</returns>
        /// <param name="round">Параметр указывает до какой точности округлять статистику.</param>
        public Dictionary<char, double> GetStatistic(int round = 3)
        {
            Dictionary<char, double> stats = new Dictionary<char, double>();
            try 
            {
                int countSymbols = this._chars.Count;
                HashSet<char> uniqueChar = new HashSet<char>(this._chars); // получаем все уникальные символы из списка символов
                Dictionary<char, int> countOfUses = this.CalculateCountOfUses(uniqueChar); //считаем статистику
                foreach (KeyValuePair<char, int> entry in countOfUses)
                {
                    stats.Add(entry.Key, Math.Round((double)entry.Value / countSymbols, round));
                }
            }
            catch
            {
                if(this._chars == null) 
                {
                    throw new Exception("Невозможно подсчитать статистику, т.к. твиты отсутствуют или в них отсутствуют буквенные символы.");
                }
            }
            return stats;
        }

        /// <summary>
        /// Считает количество использований символов в твитах.
        /// </summary>
        /// <returns>Возвращает количество использований каждого символа.</returns>
        /// <param name="unique">Список уникальных символов в твитах</param>
        private Dictionary<char, int> CalculateCountOfUses(HashSet<char> unique) 
        {
            Dictionary<char, int> countOfUses = new Dictionary<char, int>();
            foreach(char symbol in unique)
            {
                List<char> finded = this._chars.FindAll(
                delegate (char symb)
                {
                    return symb == symbol;
                }
                );
                countOfUses.Add(symbol, finded.Count);
            }
            return countOfUses;
        }

    }
}