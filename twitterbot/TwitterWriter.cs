﻿using Tweetinvi;
using System.Collections.Generic;

namespace twitterbot
{
    public class TwitterWriter : IWriter
    {

        public TwitterWriter(){}

        /// <summary>
        /// Публикует указанный текст в твиттере.
        /// </summary>
        /// <returns>Публикует твит.</returns>
        /// <param name="text">Публикуемый текст.</param>
        public void Write(string text) 
        {
            Tweet.PublishTweet(text);
        }

        /// <summary>
        /// Публикует указанные текста в твиттере
        /// </summary>
        /// <returns>Публикует твиты.</returns>
        /// <param name="texts">Список публикуемых текстов.</param>
        public void Write(List<string> texts) 
        {
            texts.ForEach(this.Write);
        }

    }
}
